Computer graphics with Stella Burckschat
----------------------------------------
1) Scratch sprites (figures) painting on paper. A set of tools (colourful pencils, paper etc.) 
   prepared by Stella. 90 min.
2) Taking a photo of paper pictures with a smartphone a copying this into the computer. 15 min.
   -- it took quite a lot of time, original images from the phone (JPG) were too big, 
      I had to resize them to 10% of original size and save as PNGs,
      in case of computer graphics course, we had to invest in a scanner
3) Working with GIMP to improve the picture. 45 min.
   -- GIMP was found too complicated and we did not have enough time (planned 45 min, at the end children have not worked
      with GIMP at all - only a little bit with Scratch editor);
      probably in the future we will have to use another program for processing the images (KolourPaint in Edubuntu)
      
      
Lesson without computer graphics
--------------------------------
1) Fix the bug - 5 small tasks. 30 min (task description in pdf/txt file in 'Fix_the_bug' directory) - it took 40 min in real case
2) Start a new project - fireworks animation as in example 'New Year Fireworks!'. 
   Handout printed 'New Year Fireworks Sprite 1 example.docx' 60 minutes.
   This task appeared to be very complex for the children - each had a different bug. Difficulties: blocks with sin/cos (hierarchy of blocks),
   mistaken between "set variable x (red blocks)" and "set position x (blue
   blocks)".
3) If there is time for this: tablets - "Connect the dots" (Functions) + Mekorama. 
   They played 30 minutes Connect the dots. Appeared to be a challenge for
   them. We should do it again.