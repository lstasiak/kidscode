1) What is algorithm - a short refresh.  10 min.
2) Mini tasks (scratch cards). 20 min.
3) Fix the bug - 5 small tasks. 30 min (task description in pdf/txt file in 'Fix_the_bug' directory)
4) Moving circle and square. 45 min (single blocks as inspiration on small sheets of paper available)
   -- kids (only girls this time) were really happy with painting,
   
5) Animate your name. 45 min
   -- we did not get to this task at all, I will take it to the next meeting
   
***
In the second course (lesson 150 minutes instead of 180 minutes as previously) we did:
Task (1) for about 10 min.
Skipped on task (2).
Task (3) took about 60 min.
Task (4) took about 50 min. and got completed.
We started task (5) for the remaining 30 minutes.