Liebe KidsCode-Eltern,

   heute haben wir mit Kindern das Konzept der Nachrichten kennengelernt und geübt. Es geht im Prinzip um die Kommunikation zwischen unterschiedlichen Figuren/Objekten im Programm. Wir haben auch mit einem "großen Projekt" eines Spieles angefangen. Jedes Kind könnte aus 4 Vorschläge ein Spiel wählen. Das Spiel wird am nächsten Samstag fertig gemacht und in letzter Stunde der Eltern präsentiert.
Die Projekte von heute (außer dem Spiel) finden Sie beiliegend.

Ein paar wichtigen Informationen:
1) Präsentation der Kinder Projekte.
Ich und die KidsCode-Kinder möchten Sie zu der Präsentation-Stunde am nächsten Samstag, den 17.03, um 11:00 in Ateliers22, ganz herzlich einladen (die Kinder bitte ganz normal um 9:00 Uhr zum Kurs bringen).

2) Fortsetzung des Kurses
     Nach Osterferien möchte ich mit dem Kurs fortfahren. Die Adresse bleibt unverändert, die Dauer des Unterrichts wird aber leicht verkürzt: wir werden uns samstags zwischen 9:00 und 11:30 treffen. Wegen vielen Feiertagen und Schulferien in kommenden Wochen werden wir uns nicht jede Woche treffen. KidsCode-Unterricht wird nämlich an folgenden Tage stattfinden: 14.04, 28.04, 05.05, 26.05, 02.06. Die Kursgebühr für diesen Kurs beträgt 185 EUR.
Sagen Sie mir bitte kurz Bescheid, ob Ihre Kinder weiter mitmachen werden (wenn nicht, wäre ich auch für kurze Rückmeldung dankbar).

3) Bitte um Feedback
     Nochmal meine herzliche Bitte um Feedback zu dem jetzigen Kurs via einen Fragebogen (benötigt 5 Minuten): https://goo.gl/forms/cNvtUY4sYkvC25042

4) Neue Kurse
     a) Nach Osterferien möchte ich auch den Kurs für Erwachsenen/Senioren anbieten: "Einführung in Programmierung" für Erwachsenen, unter dem Motto "wecke ein Kind in dir!".
     b) In Planung gibt es auch den Kurs "Computer Grafik für Anfänger" (Kinder und Erwachsenen), der von einer professionellen Grafikerin mit Erfahrung in Arbeit mit Kindern und Erwachsenen, geleitet wird.
Falls Sie jemanden kennen, der Interesse an einem von der Kurse haben könnte, leiten Sie bitte diese Infos weiter.

Schönen Sonntag und viele Grüße,
Lukasz Stasiak

-- 
Dr.-Ing. Łukasz Stasiak
www.kidscode.eu

