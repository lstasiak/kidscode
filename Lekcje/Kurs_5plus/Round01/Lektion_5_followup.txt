Liebe KidsCode-Eltern,

   nochmal möchte ich mich bei Ihnen und Ihren Kinder ganz herzlich bedanken. Wir haben guten Kurs zusammen gehabt und ich freue mich schon auf die nächste Runde (Start am 19.01.2019, über Rückmeldung - falls noch nicht gegeben - würde ich mich freuen), in der ich ein paar Verbesserungen einbringen möchte.

Für die, die noch 5 Minuten dem Feedback via Fragebogen widmen möchten, hier nochmal das Link (herzlichen Dank für Feedback): https://goo.gl/forms/MIHPa9Dha1o2jkK43

Ich wünsche Ihnen und Ihren Familien frohe Weihnachten und einen guten Rutsch ins neue Jahr!

Łukasz Stasiak

-- 
Dr.-Ing. Łukasz Stasiak
www.kidscode.eu
www.facebook.com/KidsCodeLab

