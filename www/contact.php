<?php
    // Name, e-mail and message variable
    $name = $_POST['name'];
    $email = $_POST['email'];
    $usermessage = $_POST['message'];
    $newsletter = $_POST['newsletter'];

    $sender = $email;
    $receiver = "info@kidscode.eu";

    // Message title
    $title = "Anfrage (www.KidsCode.eu)";

    // Message content
    $message = "";
    $message .= "Name: " . $name . "\n";
    $message .= "Email: " . $email . "\n";
    $message .= "Newsletter: " . $newsletter . "\n";
    $message .= "Message: " . $usermessage . "\n";
    $header = "";
    $header .= "From:" . $sender . "\n";
    $header .= "Content-Type:text/plain;charset=utf-8";

    // Send message
    $success = mail($receiver, $title, $message, $header);

    // Confirm
    if ($success){
      print "<meta http-equiv=\"refresh\" content=\"0;URL=index.html#sent_ok\">";
    }
    else{
      print "<meta http-equiv=\"refresh\" content=\"0;URL=index.html#sent_error\">";
    }
?>
